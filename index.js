/** @format */

import {AppRegistry} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
var I18n = require('react-native-i18n');
AppRegistry.registerComponent(appName, () => App);

