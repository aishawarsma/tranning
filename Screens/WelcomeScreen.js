import React, { Component } from 'react'
import { TabNavigator } from 'react-navigation'
import {stackNavigator,  DrawerNavigator} from 'react-navigation'

import {   TouchableOpacity, TextInput,ImageBackground, StyleSheet,keyboardType, TextInputValue } from 'react-native'
import { Container,View, Header, Content, Card, CardItem, Body, Text, Button,Left,Icon,Title, Right, Drawer } from 'native-base';
import I18n from 'react-native-i18n'

import { strings } from '../i18n/i18nn';

class WelcomeScreen extends React.Component{

static navigationOptions = ({navigation})=>{
let headerTitle = 'Main';
let drawerLable = 'Home';
let headerTitleStyle ={color: 'red'}
let backgroundColor = '#fff'
return{headerTitle, headerTitleStyle, backgroundColor, drawerLable};

   }


   translator(){
      I18n.defaultLocale="en"
      
     
   }

render(){

   return(
      <Container>
         <Header>
            <Left><Icon name ="ios-menu" onPress={()=> this.props.navigation.navigate('DrawerOpen')}/></Left>
            <Right>
               <Icon name ="settings" on onPress ={()=> this.props.navigation.navigate('SittingScreen')}/>
            </Right>
         </Header>
          <Content style ={{padding:20, backgroundColor : "#d0d3d4 "}}>
            <Card >
            <CardItem header bordered style ={{backgroundColor : "#d0d3d4"}}>
           <Text>{strings('WelcomeScreen.WELCOMEToHomePage')} </Text>
             </CardItem>

            <CardItem bordered style ={{backgroundColor : " #d0d3d4 "}}>
             <Body>
             <View style={{paddingBottom:50, width:300}}>
              <Button full onPress={()=>this.props.navigation.navigate('CompleteLoginScreen')}>
                <Text>{strings('WelcomeScreen.Login')}</Text>
              </Button>
             </View>
               
              <View style={{width:300}}> 
                <Button full onPress={()=>this.props.navigation.navigate('SinUpScreen') }>
                  <Text>{strings('WelcomeScreen.SinUp')}</Text>
                </Button>
             </View>
               
            </Body>
         </CardItem>       
      </Card>
    </Content> 
 </Container>
   );

 }}
 

export default WelcomeScreen
const Styles = StyleSheet.create({
    Container:
    {
       flex :1,
       backgroundColor:'#fff',
       alignItems: 'center',
       justifyContent: 'center'
    }
   });