import React, { Component } from 'react';
import { Container, Header, Content,Card,Body, CardItem, Form, Text, Item, Input, Title, Label, Button, View } from 'native-base';
import I18n from 'react-native-i18n'

import { strings } from '../i18n/i18nn';

export default class CompleteLoginScreen extends Component {



  static navigationOptions = ({navigation})=>{
    let headerTitle = 'Login';
    let drawerLable = 'Log In'
    let headerTitleStyle ={color: 'red'}
    let backgroundColor = "#d0d3d4 "
    return{headerTitle, headerTitleStyle};
}

constructor(props){
  super(props)
  this.state={
    phone:'',
    password:''
  }
}



  
 
    handleEmail = (text) => {
        this.setState({ phone: text })
       }
       handlePassword = (text) => {
          this.setState({ password: text })
       }

       loggin = (phone, pass) => {
        // alert(phone+"|"+pass)
       // if (phone == "123456"&& pass =="123"){
           
        this.props.navigation.navigate('HomeScreen')}
   // }

  render() {
    return (    

 <Container>

 <Content style={{padding:20,backgroundColor : "#d0d3d4 "}}>
   <Card>
     
     <CardItem header bordered style ={{backgroundColor : "#d0d3d4 "}}>
       <Text>{strings('CompleteLoginScreen.WELCOMEANDLOGIN')}</Text>
       </CardItem>
       <CardItem bordered style ={{backgroundColor :" #d0d3d4 "}}>
              <Body>
             
                <Item floatingLabel>
                  <Label>{strings('CompleteLoginScreen.Phone')}</Label>
                  <Input onChangeText = {this.handleEmail}/>
                </Item>
                 <Item floatingLabel last>
                   <Label>{strings('CompleteLoginScreen.Password')}</Label>
                  <Input onChangeText = {this.handlePassword} />
                  </Item>
                 <View style ={{paddingBottom :50, width :300}}>
                 <Button  full onPress = {
                  () => this.loggin(this.state.phone, this.state.password)}> 
                   <Text>{strings('CompleteLoginScreen.Login')}</Text>
                   </Button>
                   </View>
                   <View style ={{width:300}}>
                 <Button full onPress ={()=>{this.props.navigation.navigate('SinUpScreen')}}>    
                    <Text> {strings('CompleteLoginScreen.ClickToRegister')}</Text></Button> 
             
                    </View>

             </Body>
             </CardItem>
          </Card>
        </Content>
      </Container>
    );
  }
}
