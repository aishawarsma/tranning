import React, { Component } from 'react'
import { TabNavigator } from 'react-navigation'
import {stackNavigator} from 'react-navigation'

import { View,  Button,TouchableOpacity, TextInput,ImageBackground, StyleSheet,keyboardType, TextInputValue } from 'react-native'
import { Container, Header, Content, Card, CardItem, Body, Text } from 'native-base';



class UnitOneScreen extends React.Component{


    static navigationOptions = ({navigation})=>{
        const {state} = navigation;
        let headerTitle = state.params.myTitle;
        let headerTitleStyle ={color: 'red'}
      
        return{headerTitle, headerTitleStyle};
    }


    render(){
        const {state} = this.props.navigation;
        let Body = state.params.myBody
        //alert(JSON.stringify(state.params)) 
        return(
            <View style ={Styles.Container}>
            <Text>Welcome {Body} </Text>
            </View>
        );
        }
    }

  export default UnitOneScreen
    const Styles = StyleSheet.create({
        Container:
        {
           flex :1,
           backgroundColor:'#89a2a2',
           alignItems: 'center',
           justifyContent: 'center'
        }
       });