import React, { Component } from 'react'
import { TabNavigator } from 'react-navigation'
import {stackNavigator} from 'react-navigation'

import { View, TouchableOpacity, TextInput,ImageBackground, StyleSheet,keyboardType, TextInputValue } from 'react-native'
import { Container,Text, Header, Content, Card, CardItem,  Icon, Right, Body, TextContent } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { ScrollView } from 'react-native-gesture-handler';


class HomeSceen extends React.Component{


  static navigationOptions = ({navigation})=>{
    let headerTitle = 'Cards';
    let headerTitleStyle ={color: 'red'}
    return{headerTitle, headerTitleStyle};
}





render(){
  const {navigate} = this.props.navigation;
   return(
 <ScrollView>
     <Container>
       <Content>
          <Grid>
             <Row >
              <Col >
               <TouchableOpacity onPress ={()=>navigate('UnitOneScreen',{myTitle:'Unit One',myBody:'this is unit one details'})}>
                 <Card style={{ height: 100, backgroundColor = "#89a2a2" }}>
                    <CardItem >
                      <Icon active name="paper" />
                        <Text>Unit One</Text>
                         <Right>
                          <Icon name="arrow-forward" />
                         </Right>
                    </CardItem>
                 </Card>
               </TouchableOpacity>
            </Col>


            <Col >
            <TouchableOpacity onPress ={()=>navigate('UnitOneScreen',{myTitle:'Unit Tow',myBody:'this is unit 2 details'})}>
            <Card style={{ height: 100 }}>
            <CardItem >
            <Icon active name="paper" />
              <Text>Unit Tow</Text>
              <Right>
               <Icon name="arrow-forward" />
              </Right>
            </CardItem>
             </Card>
             </TouchableOpacity>
            </Col>
            </Row>
            <Row >
            <Col >
            <TouchableOpacity onPress ={()=>navigate('UnitOneScreen',{myTitle:'Unit Three ', myBody: 'This is unit 3 details'})}>
            <Card style={{ height: 100 }} >
            <CardItem button >
            <Icon active name="paper" />
              <Text>Unit Three</Text>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </CardItem>
            </Card>
             <CardItem>  
            </CardItem>
            </TouchableOpacity>
            </Col>
            <Col >  
            <TouchableOpacity onPress ={()=>navigate('UnitOneScreen',{myTitle:' Unit Four', myBody:'This is unit 4 detials'})}>
            <Card style={{ height: 100 }}>
            <CardItem >
            <Icon active name="paper" />
              <Text>Unit Four</Text>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
            </CardItem>
             </Card>
            </TouchableOpacity>
            </Col>
            </Row>
            <Row >
            <Col >
            <TouchableOpacity onPress ={()=>navigate('UnitOneScreen',{myTitle:'Unit Five',myBody:'This is unit 5 details'})}>
            <Card style={{  height: 100 }} >
               <CardItem  >
               <Icon active name="paper" />
              <Text>Unit Five</Text>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
               </CardItem>
             </Card>
            </TouchableOpacity>
           
            </Col>
            <Col >
            <TouchableOpacity onPress ={()=>navigate('UnitOneScreen',{myTitle:'Unit six',myBody:'This is unit 6 details'})}>
            <Card style={{  height: 100 }} >
               <CardItem  >
               <Icon active name="paper" />
              <Text>Unit six</Text>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
               </CardItem>
             </Card>
            </TouchableOpacity>
            
            </Col>
            </Row>


            <Row >
            <Col >
            <TouchableOpacity onPress ={()=>navigate('UnitOneScreen',{myTitle:'Unit Seven',myBody:'This is unit 7 details'})}>
            <Card style={{  height: 100 }} >
               <CardItem  >
               <Icon active name="paper" />
              <Text>Unit Seven</Text>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
               </CardItem>
             </Card>
            </TouchableOpacity>
            
            </Col>
            <Col >
            <TouchableOpacity onPress ={()=>navigate('UnitOneScreen',{myTitle:'Unit Eight',myBody:'This is unit  8 details'})}>
            <Card style={{  height: 100 }} >
               <CardItem  >
               <Icon active name="paper" />
              <Text>Unit Eight</Text>
              <Right>
                <Icon name="arrow-forward" />
              </Right>
               </CardItem>
             </Card>
            </TouchableOpacity>
           
            </Col>
            </Row>
          </Grid>
          </Content>
      </Container>
      </ScrollView>
    );
  }
}
   










export default HomeSceen
const Styles = StyleSheet.create({
    Container:
    {
       flex :1,
       backgroundColor:'#fff',
       alignItems: 'center',
       justifyContent: 'center'
    }
    }
    );