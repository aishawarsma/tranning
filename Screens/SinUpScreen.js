import React, { Component } from 'react'
import { TabNavigator } from 'react-navigation'
import {stackNavigator} from 'react-navigation'

import { View, TouchableOpacity, TextInput,ImageBackground, StyleSheet,keyboardType, TextInputValue } from 'react-native'
import { Container,Text, Header, Content, Card, CardItem, Body,Button, TextContent, Input,Form, ButtonItem, Label, Item, Title } from 'native-base';

import I18n from 'react-native-i18n'

import { strings } from '../i18n/i18nn';

class RegisterScreen extends React.Component{
  static navigationOptions = ({navigation})=>{
    let headerTitle = 'SinUp';
    let drawerLable = 'Redister';
    let headerTitleStyle ={color: 'red'}
    return{headerTitle,headerTitleStyle};
  }
   render() {
      return (    
  
   <Container>
     
   <Content padder>
     <Card>
       <CardItem header bordered>
         <Text>{strinds('SinUpScreen.Rigestration')}</Text>
         </CardItem>
         <CardItem bordered>
                <Body>
              
                  <Item floatingLabel>
                    <Label>{strings('SinUpScreen.EnteryourPhoneNomber')}</Label>
                    <Input/>
                  </Item>
                   <Item floatingLabel >
                    <Label>{strings('SinUpScreen.Password')}</Label>
                    <Input/>
                    </Item>
                    <Item floatingLabel>
                    <Label>{strings('SinUpScreen.ConferimPassword')}</Label>
                    </Item>
                 <Button full onPress = {
                    () =>this.props.navigation.navigate('CompleteLoginScreen')}> 
                     <Text>{strings('SinUpScreen.Register')}</Text>
                     </Button>
           
               </Body>
               </CardItem>
                
            
            </Card>
          </Content>
        </Container>
      );
}
}
export default RegisterScreen
const Styles = StyleSheet.create({
    Container:
    {
       flex :1,
       backgroundColor:'#fff',
       alignItems: 'center',
       justifyContent: 'center'
    }
    }
    );