import React, { Component } from 'react';
import {
  Platform,
  StyleSheet,
  Text,
  View,
} from 'react-native';
import {  TouchableOpacity, TextInput,ImageBackground, keyboardType, TextInputValue } from 'react-native'
import { Container, Header, Content, Card, CardItem,  Icon, Right, Body, TextContent } from 'native-base';
import { Col, Row, Grid } from 'react-native-easy-grid';
import { ScrollView } from 'react-native-gesture-handler';
import GridLayout from 'react-native-layout-grid';
 
export default class App extends Component {
 
  static navigationOptions = ({navigation})=>{
    let headerTitle = 'Cards';
    let headerTitleStyle ={color: 'red'}
    return{headerTitle, headerTitleStyle};
}

  
  renderGridItem = (item) => (

    


<TouchableOpacity   onPress ={()=>this.props.navigation.navigate('UnitScreen',{myTitle:item.myTitle,myBody:item.myBody})}>
                 <Card style={{ height: 100 ,backgroundColor:item.color, Icon:item.Icon}}>
                    <CardItem style={{backgroundColor:item.color}}>
                      <Icon active name={item.Icon} />
                         <Text style = {styles.name}>
                         {item.myTitle}
                          </Text>
                         <Right>
                          <Icon reverse name="arrow-forward" />
                         </Right> 
                    </CardItem>
                 </Card>
               </TouchableOpacity>
  );
 
  render() {
    

    const items = [
      {
        myTitle: "Unit 1",
        myBody: "hi 1",
        color:"#A9F5E1",
        Icon : "cart"
      },
      {
        myTitle: "Unit 2",
        myBody: "hi 2",
        color:"#A9F5D0",
        Icon : "settings"
      },
      {
        myTitle: "Unit 3",
        myBody: "hi 3",
        color: "#2ECCFA",
        Icon : "rose"
      },
      {
        myTitle: "Unit 4",
        myBody: "hi4",
        color : "#58FAF4",
        Icon : "alarm"
      },
      {
        myTitle: "Unit 5",
        myBody: "hi 5",
        color: "#2EFEC8",
        Icon : "grid"
      },
      {
        myTitle: "Unit 6",
        myBody: "hi 6",
        color : "#00FFBF",
        Icon : "bookmarks"
      },
      {
        myTitle: "Unit 7",
        myBody: "hi 7",
        color : "#01DFA5",
        Icon : "refresh"
      },
      {
        myTitle: "Unit 8",
        myBody: "hi 8",
        color : "#01DFD7",
        Icon : "flame"
      },
      {
        myTitle: "Unit 9",
        myBody: "hi 9",
        color : "#04B486",
        Icon : "settings"
      },
      {
        myTitle: "Unit 10",
        myBody: "hi 10",
        color : "#04B486",
        Icon : "pie"
      },,
      {
        myTitle: "Unit 11",
        myBody: "hi 11",
        color : "#04B486",
        Icon : "mic-off"
      },,
      {
        myTitle: "Unit 12",
        myBody: "hi 12",
        color : "#04B486",
        Icon : "film"
      },
      
      ];
     
    
    return (
      <View style={styles.container}>
          <GridLayout
            items={items}
            itemsPerRow={3}
            renderItem={this.renderGridItem}
          />
        </View>
    );
  }
}
console.disableYellowBox = true;


const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 20,
    backgroundColor: '#F5FCFF',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  flex: {
    flex: 1,
  },
  item: {
    height: 150,
    backgroundColor: '#CCCCCC',
    padding: 10,
  },
  name: {
    fontSize: 12,
    textAlign: 'center',
    //color: '#000000'
  }
});