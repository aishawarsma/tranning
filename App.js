import React, { Component } from 'react';
import { StackNavigator, createAppContainer } from 'react-navigation';
import { View,Dimentions,TouchableOpacity, TextInput,ImageBackground, StyleSheet,keyboardType, TextInputValue } from 'react-native';
import { Container, Header, Content, Card, CardItem, Body, Text } from 'native-base';
import {DrawerNavigator} from 'react-navigation';

import i18n from 'react-native-i18n';






import WelcomeScreen from './Screens/WelcomeScreen'
import LoginScreen from './Screens/LoginScreen'
import SinUpScreen from './Screens/SinUpScreen'
import CompleteLoginScreen from './Screens/CompleteLoginScreen'
import HomeScreen from './Screens/HomeScreen'
import RegisterScreen from './Screens/RegisterScreen';
import UnitScreen from './Screens/UnitScreen';
import SittingScreen from './Screens/SittingScreen';
import {Home, Register, LogIn, Sittings} from './Screens/ScreensNames';
import en from './i18n/en.json'
import ar from './i18n/ar.json'


var I18n = require('react-native-i18n');

const AppStackNavigator = new StackNavigator(
   {
      WelcomeScreen : {screen:WelcomeScreen},
      
      SinUpScreen : {screen:SinUpScreen},
      CompleteLoginScreen : {screen:CompleteLoginScreen},
      HomeScreen :{screen:HomeScreen},
      RgisterScreen :{screen:RegisterScreen},
      UnitScreen :{screen:UnitScreen},
      LoginScreen : {screen:LoginScreen},
      SittingScreen:{screen:SittingScreen},
      
   }
);   

let routConfigs = {
   Home:{
      screen: WelcomeScreen,
   },
   Register :{
      screen : SinUpScreen,
   },
   Sittings : {screen:SittingScreen},
   HomeScreen :{screen:AppStackNavigator}
   
};
let drawerNavigatorConfig ={
   intialRouteName :Home,
   
   drawerPosition: 'left',
   drawerOpenRoute: 'DrawerOpen',
   drawerCloseRoute: 'DrawerClose',
   drawerToggleRoute: 'DrawerToggle'
}
const ApDrawer = DrawerNavigator(routConfigs, drawerNavigatorConfig);
export default ApDrawer;